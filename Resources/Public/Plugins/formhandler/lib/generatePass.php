<?php
class user_generatePassword {
    var $cObj;
   
    function main($content,$conf){
        return $this->createPassword(12);
    }
    
    function createPassword($length) {
        $chars = "abcdefghijkmnopqrstuvwxyz";
        $i = 0;
        $password = "";
        while ($i <= $length) {
            $password .= $chars{mt_rand(0,strlen($chars))};
            $i++;
        }
        return $password;
    }   
}
?>