var keyStop = {
    13: "input:text, input:password", // stop enter = submit 
    end: null
};

jQuery(document).ready(function($) {

	
    //$("input:text").placeholder();  	
    $('input[placeholder], textarea[placeholder]').placeholder();

	
    //custom dropdown
    if ($('#center-select').length){
        customSelect('center-select');
    }
	
    //$('select').dropkick();
	
    //qTip
    $('.content-container p, .content-container ul').find('a[title]').qtip({
        position: {
            my: 'bottom center',
            adjust: {
                y: -35,
                x: 20,
                mouse: false
            }
        }
    });
    
    //qTip
    $('.module ul.steps').find('li[title]').qtip({
        position: {
            my: 'bottom center',
            adjust: {
                y: -50,
                x: -50,
                mouse: false
            }
        }
    });
	
    //expandables
    $('.expandables .data').hide();
	
    $('.expandables-simple .trigger').click(function(e) {
        e.preventDefault();
        $(this).next('.data').slideToggle(400);
        $(this).toggleClass('open');
    });

    //fancybox+
    $('.gallery').each(function (index) {
        $('a', $(this)).attr('rel','gallery-'+index).fancybox({
            'overlayShow'	: true,
            'margin' : 30,
            'padding' : 0,
            'autoScale' : true,
            'overlayOpacity' : '0.85',
            'changeFade' : 200,
            'overlayColor' : '#000',
            'titleShow' : true,
            'titlePosition' : 'over',
            'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
                return '<span id="fancybox-title-over">' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
            },
            'transitionIn'	: 'fade',
            'transitionOut'	: 'fade'
        });
    });
    
    $("aside div.filter input.radio").click(function() {
        $("form#filter").submit();
    });
    
    $('aside div.filter select').dropkick({
        change: function (value, label) {
            $("form#filter").submit();
        }
    });
		
    // temp hack, here to stay forever
    $('li a').each(function () {
        $(this).html( $(this).html().replace('tx_a3ekt_domain_model_', '') );
				if ( lang !== undefined && lang === 1 ) {
					switch ($(this).html()) {
						case 'dejavnost':
							$(this).html( 'activity' );
							break;
						case 'poklic':
							$(this).html( 'profession' );
							break;
						case 'dovoljenje':
							$(this).html( 'permit' );
							break;
					}
				}
    });
    $('label').each(function () {
        $(this).html( $(this).html().replace('tx_a3ekt_domain_model_', '') );
				if ( lang !== undefined && lang === 1 ) {
					switch ($(this).html()) {
						case 'dejavnost':
							$(this).html( 'activity' );
							break;
						case 'poklic':
							$(this).html( 'profession' );
							break;
						case 'dovoljenje':
							$(this).html( 'permit' );
							break;
					}
				}
    });
   
    $("div#tx-solr-pagination ul li").first().next().addClass("first");
   
    formatContentImage();
    sitemap();
    //login();
    toggleList();
   
    //table styles
    $('article table tr:even').addClass('even');

    $('#search').bind("keydown", function(event){
        var selector = keyStop[event.which];

        if(selector !== undefined && $(event.target).is(selector)) {
            event.preventDefault(); //stop event
        }
        return true;
    });
    
    autocomplete();
    
    if($('ul.gallery').length > 0)
        formatGallery();
    
    anchors();
    browserFix();
    
    //for testing
    tester();

});

function formatContentImage() {
    var caption = $("div.csc-textpic-image .csc-textpic-caption").html();
    $("div.csc-textpic-image .csc-textpic-caption").remove();
   
    $("div.csc-textpic-image a img").wrap('<ul class="gallery"/>').wrap('<figure />').wrap('<li class="first" />'); //.wrap('<a title="'+caption+'" rel="gallery-0" href="#" />');
    $("div.csc-textpic-image a img").before('<span class="magnify"></span>');
    $("div.csc-textpic-image a img").attr("alt", caption);
   
    if ( caption != null )
        $("div.csc-textpic-image a img").after('<figcaption>'+caption+'</figcaption>');
    else
        $("div.csc-textpic-image a img").after('<figcaption></figcaption>');
    
    if($(".multicolumn").first().find('li').size()=="2")
        $(".multicolumn").addClass("twoColumns");
    
    if($(".multicolumn").first().find('li').size()=="3")
        $(".multicolumn").addClass("treeColumns");
    
    if($(".multicolumn").first().find('li').size()=="4")
        $(".multicolumn").addClass("fourColumns");
    
}

function login() {
    
    $("body").removeClass("not-found");
    $("body").css("backgroundImage", "url(/typo3conf/ext/a3_ekt_template/Resources/Public/Images/bg_login.jpg)");
    $("header").css("backgroundImage", "none");
    $("header").css("boxShadow", "none");
    $(".main").css("backgroundColor", "transparent").css("color", "#fff").css("margin", "0 auto").css("width", "470px");
    $(".footer, nav, .rs, .tx-solr, .breadcrumbs").css("display","none");
    $("header .inner").css("paddingBottom", "50px");

    
    $('input#pass-text').focus(function() {
        $(this).hide();
        $('input#pass').show();
        $('input#pass').focus();
    });
    
    $('input#user').focus(function() {
        $(this).val("");
        $(this).focus();
    });
    
    $('input#pass').blur(function() {
        if($(this).val() == '') {
            $('input#pass-text').show();
            $(this).hide();
        }
    });
    
    $('input#user').blur(function() {
        if($(this).val() == '') {
            $(this).val("E-mail");
        }
    });
}

function register() {
    
    $("body").removeClass("not-found");
    $("body").css("backgroundImage", "url(/typo3conf/ext/a3_ekt_template/Resources/Public/Images/bg_login.jpg)");
    $("header").css("backgroundImage", "none");
    $("header").css("boxShadow", "none");
    $(".main").css("backgroundColor", "transparent").css("color", "#fff").css("margin", "0 auto").css("width", "470px");
    $(".footer, nav, .rs, .tx-solr, .breadcrumbs").css("display","none");
    $("header .inner").css("paddingBottom", "50px");

    
    $('input#pass-text').focus(function() {
        $(this).hide();
        $('input#pass').show();
        $('input#pass').focus();
    });
    
    $('input#pass-text2').focus(function() {
        $(this).hide();
        $('input#pass2').show();
        $('input#pass2').focus();
    });
    
    $('input#user').focus(function() {
        $(this).val("");
    //$(this).focus();
    });
    
    $('input#pass').blur(function() {
        if($(this).val() == '') {
            $('input#pass-text').show();
            $(this).hide();
        }
    });
    
    $('input#pass2').blur(function() {
        if($(this).val() == '') {
            $('input#pass-text2').show();
            $(this).hide();
        }
    });
    
    $('input#user').val("Enter your email address");
    $('input#user').blur(function() {
        if($(this).val() == '') {
            $(this).val("Enter your email address");
        }
    });
}

function sitemap() {
    $("div.csc-sitemap ul li ul li a").prepend('<span>&rsaquo; </span>');
    $("div.csc-sitemap ul li ul li ul li a").prepend('- ');
    $("div.csc-sitemap ul li ul li ul li a span").remove();
}

function formatGallery() {
    $('ul.gallery li').first().addClass('first');
}

function toggleList() {
    $("a.toggleList").click(function() {
       
        $(this).prev("ul").animate({
            height: $(this).prev("ul").get(0).scrollHeight + "px"
        }, 500, function() {
            $(this).next("a.toggleList").css("display","none");
            $(this).next().next("a.toggleListOpen").css("display","block");
        });
    });
   
    $("a.toggleListOpen").click(function() {
       
        $(this).prev().prev("ul").animate({
            height: "128px"
        }, 500, function() {
            $(this).next("a.toggleList").css("display","block");
            $(this).next().next("a.toggleListOpen").css("display","none");
        });
    });
}

function autocomplete() {
    // ekt search autocomplete
    if ($('.tx-solr-q-eugo').length) {
        var suggestWidth = '527px';
        if ( $('#content-search').length) suggestWidth = '645px';
        if ( $('body.small-suggest').length) suggestWidth = '365px';
        if ( $('form.narrow-search').length) suggestWidth = '364px';
        if ( $('form.wide-search').length) suggestWidth = '644px';
            
        //responsive design
        if($(window).width()<=768) suggestWidth = '370px';
        if($(window).width()<=480) suggestWidth = '245px';
			
        $('.tx-solr-q-eugo').autocomplete_eugo(tx_a3ekt_solrUrl,{
            dataType: 'json',
            minChars: 3,
            matchContains: "word",
            autoFill: false,
            width: suggestWidth,
            selectFirst: false,
            scrollHeight : '500px',
            parse: function(data) {
                var rows = new Array();
                for(var i=0; i<data.length; i++){
                    
                    var dataUrl = data[i].url;
                    
                    if(dataUrl[0]!="/")
                        dataUrl = "/"+data[i].url;
                    
                    rows[i] = {
                        data:data[i], 
                        value:data[i].title, 
                        result:data[i].title, 
                        url:dataUrl
                    };
                }
                return rows;
            },
            formatItem: function(row) {
                return row.title + ' <span class="cat"><span>/</span> ' + row.type + '</span>';
            },
            formatMatch: function(row, i, max) {
                return row.type;
            },
            formatResult: function(row) {
                return row.type;
            }
        });
    }
}

//for testing
function tester() {
    if(document.domain == "dev-ekt.aeris3.si" || document.domain == "dev-sbp.aeris3.si" || document.domain == "ekt.aeris3.si" || document.domain == "sbp.aeris3.si" || document.domain == "ekt.localhost" || document.domain == "sbp.localhost") {
        $("body").append('<div id="tester" style="color:red; position:absolute; top:0px; left:0px; border: 2px solid #fff; background-color:black; padding:5px;">tester</div>');
        $("div#tester").html("Window size:<br />" + $(window).width() + " x " + $(window).height() + "px");
    }
}

function anchors() {
    $("a[name]").html("");
}

function browserFix() {
    if ( $.browser.msie && $.browser.version < 9) {
        $("ul.treeColumns li").each(function() {
            $(this).find("a").last().addClass("more2");
        });
        
        $("a.more").css("backgroundPosition","-1205px -227px").css("paddingLeft", "20px");
        
        if ($.browser.version == 7) {
            $(".filter .radios input").css("float", "left");
            $(".calculation dt.total").addClass("clear");
            $("form.content-search").parent().parent().css("marginBottom","30px");
        }
        
    }
    
    //if($.browser.opera && $.browser.version <= 11) {}
    
    //if($.browser.opera && $.browser.version <= 11) {}
    
    //if($.browser.mozilla && $.browser.version <= 3.5) {}
}
