/* TEAM */
	Project Leader: Bogan Stelcar
	Contact: bstelcar [at] aeris3.si
	From: Maribor, Slovenia
	
	Developer: Jernej Zorec
	Contact: jzorec [at] aeris3.si
	Twitter: @nej2222
	From: Maribor, Slovenia

	Developer: Robert Ferencek
	Contact: rferencek [at] aeris3.si
	Twitter: @robertferencek
	From: Maribor, Slovenia
	
	Developer: Gregor Kirbis
	Contact: gkribis [at] aeris3.si
	From: Maribor, Slovenia

	Project Leader: Boris Saletic
	From: Ljubljana, Slovenia
	
	Project Leader: Zdravko Cule
	From: Ljubljana, Slovenia
	
	Editor: Adela Strukelj
	From: Ljubljana, Slovenia
	
	Editor: Ana Oblak
	From: Ljubljana, Slovenia
	
	Editor: Tina Kulis
	From: Ljubljana, Slovenia
	
	Editor: Ziga Pavsic
	From: Ljubljana, Slovenia
	
	Web design: Innovatif Team
	Contact: info [at] innovatif.com
	From:Ljubljana, Slovenia

/* SITE */
	Standards: HTML5, CSS3, Responsive design
	Language: English / Slovenian
	Components: TYPO3, Modernizr, jQuery, Solr, Varnish, Extbase, Fluid
	Software: Textmate, Netbeans, Good old console based text editor
