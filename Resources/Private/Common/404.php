<?php

//function returns pid of 404 page depending on domain
function get404() {
    $RootPID = array(
        'ekt.localhost' => '82',
        'sbp.localhost' => '320',
        'dev-ekt.aeris3.si' => '82',
        'dev-sbp.aeris3.si' => '320',
        'ekt.aeris3.si' => '82',
        'sbp.aeris3.si' => '320',
        'test.ekt.eugo.sigov.si' => '82',
        'app.eugo.sigov.si' => '320',
        'www.eugo.gov.si' => '320',
    );
    
    return "/?id=".$RootPID[$_SERVER['HTTP_HOST']];
}

?>
