<?php

$RootPID = array(
    'ekt.localhost' => '1',
    'sbp.localhost' => '294',
    'dev-ekt.aeris3.si' => '1',
    'dev-sbp.aeris3.si' => '294',
    'ekt.aeris3.si' => '1',
    'sbp.aeris3.si' => '294',
    'test.ekt.eugo.sigov.si' => '1',
    'app.eugo.sigov.si' => '294',
    'www.eugo.gov.si' => '294',
);

//custom function for exbase - encode url
function user_encodeSpURL_postProc(&$params, &$ref) {
    //if english language
    if ( $GLOBALS['TSFE']->sys_language_uid == 1 || strstr($GLOBALS["_SERVER"]["REQUEST_URI"], '/en/') ) {
		$params['URL'] = str_replace('action/show/Dejavnost/', 'showActivity/', $params['URL']);
        $params['URL'] = str_replace('action/show/Poklic/', 'showProfession/', $params['URL']);
        $params['URL'] = str_replace('action/show/DrugiPogoj/', 'showOtherCondition/', $params['URL']);
        $params['URL'] = str_replace('action/show/Dovoljenje/', 'showPermit/', $params['URL']);
        $params['URL'] = str_replace('action/show/CezmejnoOpravljenjePoklica/', 'showCrossborderProvisionOfProfession/', $params['URL']);
        $params['URL'] = str_replace('action/show/DovoljenjeVTujini/', 'showPermitAbroad/', $params['URL']);
        $params['URL'] = str_replace('action/show/Sektor/', 'showSector/', $params['URL']);
        $params['URL'] = str_replace('action/show/CezmejnoDovoljenje/', 'showCrossborderPermit/', $params['URL']);

		$params['URL'] = str_replace('dejavnost/', 'activity/', $params['URL']);
		$params['URL'] = str_replace('poklic/', 'profession/', $params['URL']);
		$params['URL'] = str_replace('drugiPogoj/', 'otherCondition/', $params['URL']);
		$params['URL'] = str_replace('dovoljenje/', 'permit/', $params['URL']);
		$params['URL'] = str_replace('dovoljenjeVTujini/', 'permitAbroad/', $params['URL']);
		$params['URL'] = str_replace('sektor/', 'sector/', $params['URL']);
		$params['URL'] = str_replace('cezmejnoOpravljanjePoklica/', 'crossborderProvisionOfProfession/', $params['URL']);
        $params['URL'] = str_replace('cezmejnoDovoljenje/', 'crossBorderPermit/', $params['URL']);
      
    //si language
    } else {
    
        $params['URL'] = str_replace('action/show/Dejavnost/', 'prikaziDejavnost/', $params['URL']);
        $params['URL'] = str_replace('action/show/Poklic/', 'prikaziPoklic/', $params['URL']);
        $params['URL'] = str_replace('action/show/DrugiPogoj/', 'prikaziDrugiPogoj/', $params['URL']);
        $params['URL'] = str_replace('action/show/Dovoljenje/', 'prikaziDovoljenje/', $params['URL']);
        $params['URL'] = str_replace('action/show/CezmejnoOpravljenjePoklica/', 'prikaziCezmejnoOpravljanjePoklica/', $params['URL']);
        $params['URL'] = str_replace('action/show/DovoljenjeVTujini/', 'prikaziDovoljenjeVTujini/', $params['URL']);
        $params['URL'] = str_replace('action/show/Sektor/', 'prikaziSektor/', $params['URL']);
        $params['URL'] = str_replace('action/show/CezmejnoDovoljenje/', 'prikaziCezmejnoDovoljenje/', $params['URL']);

    }
}

//custom function for exbase - decode url
function user_decodeSpURL_preProc(&$params, &$ref) {
    //if english language
     if ( $GLOBALS['TSFE']->sys_language_uid == 1 || strstr($GLOBALS["_SERVER"]["REQUEST_URI"], '/en/') ) {
		$params['URL'] = str_replace('showActivity/', 'action/show/Dejavnost/', $params['URL']);
        $params['URL'] = str_replace('showProfession/', 'action/show/Poklic/', $params['URL']);
        $params['URL'] = str_replace('showOtherCondition/', 'action/show/DrugiPogoj/', $params['URL']);
        $params['URL'] = str_replace('showPermit/', 'action/show/Dovoljenje/', $params['URL']);
        $params['URL'] = str_replace('showCrossborderProvisionOfProfession/', 'action/show/CezmejnoOpravljenjePoklica/', $params['URL']);
        $params['URL'] = str_replace('showPermitAbroad/', 'action/show/DovoljenjeVTujini/', $params['URL']);
        $params['URL'] = str_replace('showSector/', 'action/show/Sektor/', $params['URL']);
        $params['URL'] = str_replace('showCrossborderPermit/', 'action/show/CezmejnoDovoljenje/', $params['URL']);

		$params['URL'] = str_replace('activity/', 'dejavnost/', $params['URL']);
		$params['URL'] = str_replace('profession/', 'poklic/', $params['URL']);
		$params['URL'] = str_replace('otherCondition/', 'drugiPogoj/', $params['URL']);
		$params['URL'] = str_replace('permit/', 'dovoljenje/', $params['URL']);
		$params['URL'] = str_replace('permitAbroad/', 'dovoljenjeVTujini/', $params['URL']);
		$params['URL'] = str_replace('sector/', 'sektor/', $params['URL']);
		$params['URL'] = str_replace('crossborderProvisionOfProfession/', 'cezmejnoOpravljanjePoklica/', $params['URL']);
        $params['URL'] = str_replace('crossBorderPermit/', 'cezmejnoDovoljenje/', $params['URL']);

    //si language
    } else {
    
        $params['URL'] = str_replace('prikaziDejavnost/', 'action/show/Dejavnost/', $params['URL']);
        $params['URL'] = str_replace('prikaziPoklic/', 'action/show/Poklic/', $params['URL']);
        $params['URL'] = str_replace('prikaziDrugiPogoj/', 'action/show/DrugiPogoj/', $params['URL']);
        $params['URL'] = str_replace('prikaziDovoljenje/', 'action/show/Dovoljenje/', $params['URL']);
        $params['URL'] = str_replace('prikaziCezmejnoOpravljanjePoklica/', 'action/show/CezmejnoOpravljenjePoklica/', $params['URL']);
        $params['URL'] = str_replace('prikaziDovoljenjeVTujini/', 'action/show/DovoljenjeVTujini/', $params['URL']);
        $params['URL'] = str_replace('prikaziSektor/', 'action/show/Sektor/', $params['URL']);
        $params['URL'] = str_replace('prikaziCezmejnoDovoljenje/', 'action/show/CezmejnoDovoljenje/', $params['URL']);
    }
}

    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = array(
        'encodeSpURL_postProc' => array('user_encodeSpURL_postProc'),
        'decodeSpURL_preProc' => array('user_decodeSpURL_preProc'),
        '_DEFAULT' => array(
            'init' => array(
                'enableCHashCache' => true,
                'appendMissingSlash' => 'ifNotFile,redirect',
                'adminJumpToBackend' => true,
                'enableUrlDecodeCache' => true,
                'enableUrlEncodeCache' => true,
                'emptyUrlReturnValue' => '/',
            ),
            'pagePath' => array(
                'type' => 'user',
                'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
                'spaceCharacter' => '-',
                'languageGetVar' => 'L',
                'rootpage_id' => $RootPID[$_SERVER['HTTP_HOST']],
            ),
            'fileName' => array(
                'defaultToHTMLsuffixOnPrev' => 0,
                'acceptHTMLsuffix' => 1,
                'index' => array(
                    'print' => array(
                        'keyValues' => array(
                            'type' => 98,
                        )
                    )
                )
            ),
            'preVars' => array(
                0 => array(
                    'GETvar' => 'L',
                    'valueMap' => array(
                        'si' => '0',
                        'en' => '1'
                    ),
                    'defaultValue' => 'en',
                    'noMatch' => 'bypass',
                ),
                1 => array(
                    'GETvar' => 'no_cache',
                    'valueMap' => array(
                        'nc' => '1'
                    ),
                    'noMatch' => 'bypass'
                ),
            ),
            'postVarSets' => array(
                '_DEFAULT' => array(
                    'dejavnost' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[dejavnost]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_dejavnost',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'poklic' => array(
                        0 => array(
                            'GETvar' => 'tx_a3ekt_template[poklic]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_poklic',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            )
                        ),
                    ),
                    'drugiPogoj' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[drugiPogoj]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_drugipogoj',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'dovoljenje' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[dovoljenje]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_dovoljenje',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'cezmejnoOpravljanjePoklica' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[cezmejnoOpravljenjePoklica]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_cezmejnoopravljenjepoklica',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'dovoljenjeVTujini' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[dovoljenjeVTujini]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_dovoljenjevtujini',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'sektor' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[sektor]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_sektor',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'cezmejnoDovoljenje' => array(
                        array(
                            'GETvar' => 'tx_a3ekt_template[cezmejnoDovoljenje]',
                            'lookUpTable' => array(
                                'table' => 'tx_a3ekt_domain_model_cezmejnodovoljenje',
                                'id_field' => 'uid',
                                'alias_field' => 'uid',
                                'addWhereClause' => '',
                                'useUniqueCache' => '1',
                                'useUniqueCache_conf' => array(
                                    'strtolower' => '1',
                                    'spaceCharacter' => '-'
                                )
                            ),
                        ),
                    ),
                    'action' => array(
                        0 => array(
                            'GETvar' => 'tx_a3ekt_template[action]',
                        ),
                        1 => array(
                            'GETvar' => 'tx_a3ekt_template[controller]',
                        ),
                    ),
                )
            )
        )
    );

?>