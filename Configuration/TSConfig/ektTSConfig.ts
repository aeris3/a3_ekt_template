
RTE.classesAnchor >
RTE.classes >

RTE.default {
	showTagFreeClasses = 1
	contentCSS = EXT:a3_ekt_template/Resources/Public/Styles/rte.css
    	useCSS = 1

	FE.proc.allowedClasses < RTE.default.proc.allowedClasses

	showButtons (
	     	class,blockstyle,formatblock,strong,orderedlist,unorderedlist,outdent,indent,link,unlink,table,tableproperties,chMode,removeformat,toggleborders,removeformat,copy,cut,paste
	)

    buttons.formatblock.removeItems = h5,h6,pre,address,blockquote,div,header,footer,section,aside,article,nav
	
	proc {
		allowedClasses := addToList(seznam,tabela,phone,phone_en,online,pdf,external,txt,xls,ppt,doc)
		entryHTMLparser_db.allowTags < RTE.default.proc.allowTags
	}
}

TCEMAIN.clearCacheCmd = pages
