mod.wizards.newContentElement.renderMode = tabs

RTE.classesAnchor >
RTE.classes >

RTE.default {
	showTagFreeClasses = 1
	contentCSS = EXT:a3_ekt_template/Resources/Public/Styles/rte.css
    	useCSS = 1

	FE.proc.allowedClasses < RTE.default.proc.allowedClasses

	showButtons (
	     	class,blockstyle,textstyle,formatblock,strong,orderedlist,unorderedlist,outdent,indent,link,unlink,inserttag,table,tableproperties,chMode,removeformat,toggleborders,removeformat,copy,cut,paste
	)

    buttons.formatblock.removeItems = h5,h6,pre,address,blockquote,div,header,footer,section,aside,article,nav
    buttons.inserttag.denyTags = abbr, acronym, address, b, big, blockquote, cite, code, div, em, fieldset, font, h1, h2, h3, h4, h5, h6, i, legend, li, ol, p, pre, q, small, span, strong, sub, sup, table, tt, ul, strike, u
	buttons.inserttag.allowedAttribs = name
	
	proc {
		allowedClasses := addToList(seznam,links-support,tabela,phone,phone_en,online,pdf,external,txt,xls,ppt,doc)
		entryHTMLparser_db.allowTags < RTE.default.proc.allowTags
	}
}

RTE.default.tx_linkhandler {
	tt_news >
	ekt {
		label=EKT
		listTables=tx_a3ekt_domain_model_dejavnost,tx_a3ekt_domain_model_poklic,tx_a3ekt_domain_model_dovoljenje,tx_a3ekt_domain_model_cezmejnodovoljenje,tx_a3ekt_domain_model_dovoljenjevtujini,tx_a3ekt_domain_model_sektor
	}
}

mod.tx_linkhandler {
	tt_news >
	ekt {
		label=EKT
		listTables=tx_a3ekt_domain_model_dejavnost,tx_a3ekt_domain_model_poklic,tx_a3ekt_domain_model_dovoljenje,tx_a3ekt_domain_model_cezmejnodovoljenje,tx_a3ekt_domain_model_dovoljenjevtujini,tx_a3ekt_domain_model_sektor
	}
}

mod.SHARED{
        defaultLanguageFlag = si.gif
        defaultLanguageLabel = Slovensko
}