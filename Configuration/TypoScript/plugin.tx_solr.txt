
config.index_enable = 1

plugin.tx_solr.solr.host = {$solr.host}
plugin.tx_solr.solr.port = {$solr.port}
plugin.tx_solr.solr.path = {$solr.path}
plugin.tx_solr.solr.scheme = {$solr.scheme}

plugin.tx_solr {
  cssFiles {
    results     = EXT:a3_ekt_template/Resources/Public/Styles/solr_results.css
    pagebrowser = EXT:a3_ekt_template/Resources/Public/Styles/pagebrowse.css
    suggest     = EXT:a3_ekt_template/Resources/Public/Styles/solr_jquery-ui.custom.css
  }
}

# main page search form
lib.primarySearch < plugin.tx_solr_pi_search
lib.primarySearch {
  templateFiles.search = EXT:a3_ekt_template/Resources/Public/Partials/SolrSearchFront.html
  search.targetPage = {$basics.searchPid}
}

# small search form widget
lib.widgetSearch < plugin.tx_solr_pi_search
lib.widgetSearch {
  templateFiles.search = EXT:a3_ekt_template/Resources/Public/Partials/SolrSearchWidget.html
  search.targetPage = {$basics.searchPid}
}

plugin.tx_solr_pi_search.templateFiles.search = EXT:a3_ekt_template/Resources/Public/Partials/SolrSearchInner.html
plugin.tx_solr_pi_search.search.targetPage = {$basics.searchPid}

plugin.tx_solr_pi_results.templateFiles.pagebrowser = EXT:a3_ekt_template/Resources/Public/Partials/SolrPagebrowser.html

plugin.tx_solr.suggest = 1 
plugin.tx_solr.search.results.resultsHighlighting = 1 
plugin.tx_solr.search.sorting = 0 
plugin.tx_solr.search.spellchecking = 1 
plugin.tx_solr.search.faceting = 1 
plugin.tx_solr.index.files = 0

plugin.tx_solr.index.queue.Dejavnost = 1
plugin.tx_solr.index.queue.Dejavnost {
  table = tx_a3ekt_domain_model_dejavnost
  
  fields {
    title = naziv
    content = SOLR_CONTENT
    content {
      field = opis
    }
    
    // the special SOLR_RELATION content object resolves relations
    category_stringM = SOLR_RELATION
    category_stringM {
      localField = dejavnost_po_e_k_t
      multiValue = 0
    }
    
    kljucnebesede_stringM = SOLR_MULTIVALUE
    kljucnebesede_stringM {
      field = kljucne_besede
    }
    
    pravnepodlage_stringM = SOLR_RELATION
    pravnepodlage_stringM {
      localField = pravne_podlage
      multiValue = 1
    }
  
    // build the URL through typolink, make sure to use returnLast = url
    url = TEXT
    url {
      typolink.parameter = {$plugin.tx_a3ekt.settings.dejavnosti}
      typolink.additionalParams = &L={field:__solr_index_language}&tx_a3ekt_template%5Bdejavnost%5D={field:uid}&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dejavnost
      typolink.additionalParams.insertData = 1
      typolink.returnLast = url
      typolink.useCacheHash = 1
    }
  }
}

plugin.tx_solr.index.queue.Poklic = 1
plugin.tx_solr.index.queue.Poklic {
  table = tx_a3ekt_domain_model_poklic
  
  fields {
    title = naziv
    content = SOLR_CONTENT
    content {
      field = opis
    }
    
    kljucnebesede_stringM = SOLR_MULTIVALUE
    kljucnebesede_stringM {
      field = kljucne_besede
    }
    
    // build the URL through typolink, make sure to use returnLast = url
    url = TEXT
    url {
      typolink.parameter = {$plugin.tx_a3ekt.settings.poklici}
      typolink.additionalParams = &L={field:__solr_index_language}&tx_a3ekt_template%5Bpoklic%5D={field:uid}&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Poklic
      typolink.additionalParams.insertData = 1
      typolink.returnLast = url
      typolink.useCacheHash = 1
    }
  }
}

plugin.tx_solr.index.queue.Dovoljenje = 1
plugin.tx_solr.index.queue.Dovoljenje {
  table = tx_a3ekt_domain_model_dovoljenje
  
  fields {
    title = naziv
    content = SOLR_CONTENT
    content {
      field = opis
    }
    
    # works only with the n:1 relation patch for solr!
    vloge_stringM = SOLR_RELATION
    vloge_stringM {
      localField = vloge
      foreignField = dovoljenje
      multiValue = 1
    }
    
    # works only with the n:1 relation patch for solr!
    prilogekvlogi_stringM = SOLR_RELATION
    prilogekvlogi_stringM {
      localField = priloge_k_vlogi
      foreignField = dovoljenje
      multiValue = 1
    }
    
    # works only with the n:1 relation patch for solr!
    registerdovoljenje_stringM = SOLR_RELATION
    registerdovoljenje_stringM {
      localField = register_dovoljenj
      foreignField = dovoljenje
      multiValue = 1
    }
    
    // build the URL through typolink, make sure to use returnLast = url
    url = TEXT
    url {
      typolink.parameter = {$plugin.tx_a3ekt.settings.dovoljenja}
      typolink.additionalParams = &L={field:__solr_index_language}&tx_a3ekt_template%5Bdovoljenje%5D={field:uid}&tx_a3ekt_template%5Baction%5D=show&tx_a3ekt_template%5Bcontroller%5D=Dovoljenje
      typolink.additionalParams.insertData = 1
      typolink.returnLast = url
      typolink.useCacheHash = 1
    }
  }
}

# set search fields
plugin.tx_solr.search.query.fields = content^40.0, title^50.0, keywords^20.0, kljucnebesede_stringM^30.0, tagsH1^5.0, tagsH2H3^3.0, tagsH4H5H6^2.0, tagsInline^1.0, pravnepodlage_stringM^10.0, vloge_stringM^10.0, prilogekvlogi_stringM^10.0, registerdovoljenje_stringM^10.0, category_stringM^20.0

page.headerData.321 = TEXT    
page.headerData.321.value (
<script type="text/javascript">
/*<![CDATA[*/
var tx_a3ekt_solrUrl = '/index.php?eID=tx_a3ekt_solr&id={$basics.rootPid}&L=1';
var lang = 1;
/*]]>*/
</script>
)
[globalVar = GP:L = 0]
page.headerData.321.value (
<script type="text/javascript">
/*<![CDATA[*/
var tx_a3ekt_solrUrl = '/index.php?eID=tx_a3ekt_solr&id={$basics.rootPid}&L=0';
var lang = 0;
/*]]>*/
</script>
)
[global]
[globalVar = GP:L = 1]
page.headerData.321.value (
<script type="text/javascript">
/*<![CDATA[*/
var tx_a3ekt_solrUrl = '/index.php?eID=tx_a3ekt_solr&id={$basics.rootPid}&L=1';
var lang = 1;
/*]]>*/
</script>
)
[global]