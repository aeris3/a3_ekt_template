lib.logo = HTML
lib.logo.value (
<a class="url fn org" href="/">
    <img src="typo3conf/ext/a3_ekt_template/Resources/Public/Images/logo-beta.png" class="logo-print" alt="Doing Business in Slovenia"> 
</a>
)
lib.logo >
lib.logo = TEXT
lib.logo.value = &nbsp;
lib.logo.typolink.parameter = {$basics.rootPid}
lib.logo.typolink.ATagParams = class="logo-print" alt="Doing Business in Slovenia"
lib.logo.typolink.additionalParams = &L=1
[globalVar = GP:L = 0]
lib.logo.typolink.additionalParams = &L=0
[global]
[globalVar = GP:L = 1]
lib.logo.typolink.additionalParams = &L=1
[global]

lib.sloLink = HTML
lib.sloLink.value (
<a href="http://www.gov.si/" class="rs" target="_blank">
	<img src="typo3conf/ext/a3_ekt_template/Resources/Public/Images/rs.png" alt="Republika Slovenija" width="134" height="42" />
	Republika Slovenija
</a>
)

lib.family = HTML
lib.family.value (
<li><a href="http://ec.europa.eu/internal_market/eu-go/" target="_blank"><img src="typo3conf/ext/a3_ekt_template/Resources/Public/Images/logo-eugo.png" alt="eugo" /><span>Part of the <br />EUGO network</span></a></li>
<li><a href="http://ec.europa.eu/esf/" target="_blank"><img src="typo3conf/ext/a3_ekt_template/Resources/Public/Images/logo-fund.png" alt="European Social Fund" /><span>European <br />Social Fund</span></a></li>
<li class="extra"><a href="javascript:void(0)"><img src="typo3conf/ext/a3_ekt_template/Resources/Public/Images/investing.png" alt="European Social Fund" /></a></li>
)