/*
SQLyog Enterprise - MySQL GUI v5.32
Host - 5.1.30-community-log : Database - dev_fluid
*********************************************************************
Server version : 5.1.30-community-log
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Data for the table `backend_layout` */

insert  into `backend_layout`(`uid`,`pid`,`t3ver_oid`,`t3ver_id`,`t3ver_wsid`,`t3ver_label`,`t3ver_state`,`t3ver_stage`,`t3ver_count`,`t3ver_tstamp`,`t3ver_move_id`,`t3_origuid`,`tstamp`,`crdate`,`cruser_id`,`hidden`,`deleted`,`sorting`,`title`,`description`,`config`,`icon`) values (1,3,0,0,0,'',0,0,0,0,0,0,1330351006,1330096866,1,0,0,256,'Home','','backend_layout {\r\n        colCount = 2\r\n        rowCount = 1\r\n        rows {\r\n                1 {\r\n                        columns {\r\n                                1 {\r\n                                        name = Main Column\r\n                                        colPos = 1\r\n                                }\r\n                                2 {\r\n                                        name = Right Column\r\n                                        colPos = 2\r\n                                }\r\n                        }\r\n                }\r\n        }\r\n}','');
insert  into `backend_layout`(`uid`,`pid`,`t3ver_oid`,`t3ver_id`,`t3ver_wsid`,`t3ver_label`,`t3ver_state`,`t3ver_stage`,`t3ver_count`,`t3ver_tstamp`,`t3ver_move_id`,`t3_origuid`,`tstamp`,`crdate`,`cruser_id`,`hidden`,`deleted`,`sorting`,`title`,`description`,`config`,`icon`) values (2,3,0,0,0,'',0,0,0,0,0,0,1330350996,1330350916,1,0,0,128,'1-Column','','backend_layout {\r\n	colCount = 2\r\n	rowCount = 1\r\n	rows {\r\n		1 {\r\n			columns {\r\n				1 {\r\n					name = Main Column\r\n					colspan = 2\r\n					colPos = 1\r\n				}\r\n			}\r\n		}\r\n	}\r\n}\r\n','');
insert  into `backend_layout`(`uid`,`pid`,`t3ver_oid`,`t3ver_id`,`t3ver_wsid`,`t3ver_label`,`t3ver_state`,`t3ver_stage`,`t3ver_count`,`t3ver_tstamp`,`t3ver_move_id`,`t3_origuid`,`tstamp`,`crdate`,`cruser_id`,`hidden`,`deleted`,`sorting`,`title`,`description`,`config`,`icon`) values (3,3,0,0,0,'',0,0,0,0,0,0,1330351017,1330350925,1,0,0,64,'2-Columns','','backend_layout {\r\n        colCount = 2\r\n        rowCount = 1\r\n        rows {\r\n                1 {\r\n                        columns {\r\n                                1 {\r\n                                        name = Main Column\r\n                                        colPos = 1\r\n                                }\r\n                                2 {\r\n                                        name = Right Column\r\n                                        colPos = 2\r\n                                }\r\n                        }\r\n                }\r\n        }\r\n}','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;