<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

if ( !function_exists(user_reverseMenuItemProcFunc) ) {
	function user_reverseMenuItemProcFunc($I,$conf)	{
		return array_reverse($I);
	}	
}


t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'EKT Template');


?>